#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>
#include <time.h>

#define PORT 8080

// deklarasi struct
struct acces
{
    char name[10000];
    char pass[10000];
};

// untuk memeriksa permission
int checkPermission(char *uname, char *pass)
{
    FILE *file;
    struct acces user;
    int id, mark = 0;

    file = fopen("../database/databases/user.dat", "rb");

    while (1)
    {
        fread(&user, sizeof(user), 1, file);
        if (strcmp(user.name, uname) == 0)
            if (strcmp(user.pass, pass) == 0)
                mark = 1;
        if (feof(file))
            break;
    }

    fclose(file);

    if (mark == 0)
    {
        printf("Not Allowed: Not have permission\n");
        return 0;
    }
    else
        return 1;
}

int main(int argc, char *argv[])
{
    int allowed = 0;
    int id_user = geteuid();
    char used_db[1000];

    if (geteuid() == 0)
        allowed = 1;
    else
    {
        int id = geteuid();
        allowed = checkPermission(argv[2], argv[4]);
    }

    if (allowed == 0)
        return 0;

    FILE *file;
    char loc[10000];
    snprintf(loc, sizeof loc, "../database/log/log%s.log", argv[2]);
    file = fopen(loc, "rb");
    char buff[20000];
    char cmd[100][10000];
    int mark = 0;

    while (fgets(buff, 20000, file))
    {
        char *token;
        char temp_buff[20000];
        snprintf(temp_buff, sizeof temp_buff, "%s", buff);
        token = strtok(temp_buff, ":");
        int i = 0;

        while (token != NULL)
        {
            strcpy(cmd[i], token);
            i++;
            token = strtok(NULL, ":");
        }

        char *b = strstr(cmd[4], "USE");
        if (b != NULL)
        {
            char *tokens;
            char temp_cmd[20000];
            strcpy(temp_cmd, cmd[4]);
            tokens = strtok(temp_cmd, "; ");
            int j = 0;

            char use_cmd[100][10000];

            while (tokens != NULL)
            {
                strcpy(use_cmd[j], tokens);
                j++;
                tokens = strtok(NULL, "; ");
            }

            char databaseTarget[20000];
            if (strcmp(use_cmd[1], argv[5]) == 0)
                mark = 1;
            else
                mark = 0;
        }
        if (mark == 1)
            printf("%s", buff);
    }
    fclose(file);
}