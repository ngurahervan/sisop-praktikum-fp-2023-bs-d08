# sisop-praktikum-modul-2-2023-BS-D08
> Kelompok D08

***

## Anggota Kelompok
1. I Gusti Ngurah Ervan Juli Ardana (5025211205)
2. Ghifari Maaliki Syafa Syuhada (5025211158)
3. Elmira Farah Azalia (5025211197)

## A. Autentikasi
Autentikasi untuk user dicapai dengan fungsi `autentikasi` dimana data
login user disimpan di file `user.dat`. Maka setiap login akan dilakukan
pencocokkan kredensial username dan password.

fungsi `autentikasi`:  
```
int autentikasi(char *username, char *password) {
  FILE *file;
  struct acces client;
  int id, found = 0;
  file = fopen("../database/databases/user.dat", "rb");

  while (fread(&client, sizeof(client), 1, file)) {
    if (strcmp(client.name, username) == 0 && strcmp(client.pass, password) == 0) {
      found = 1;
      break;
    }
  }

  fclose(file);

  if (found == 0) {
    printf("Not Allowed: No permission\n");
    return 0;
  } else {
    return 1;
  }
}
```

## B. Autorisasi
Untuk autorisasi database, permissionnya dapat diberikan oleh root
dengan fungsi `insertPermission`. Ketika
user ingin mengakses suatu database, maka fungsi `DBPermission` akan
memeriksa terlebih dahulu apakah terdapat pasangan nama dan database
yang ingin diakses pada file `permission.dat`

fungsi `insertPermission`:  
```
void insertPermission(char *name, char *database)
{
  struct permission_db user;

  strcpy(user.name, name);
  strcpy(user.database, database);

  printf("%s %s\n", user.name, user.database);

  char fname[] = {"databases/permission.dat"};
  FILE *file;
  file = fopen(fname, "ab");
  fwrite(&user, sizeof(user), 1, file);
  fclose(file);
}
```

fungsi `DBPermission`:  
```
int DBPermission(char *name, char *database)
{
  FILE *file;
  struct permission_db user;
  int id, mark = 0;
  printf("name = %s  database = %s", name, database);
  file = fopen("../database/databases/permission.dat", "rb");
  while (1)
  {
    fread(&user, sizeof(user), 1, file);
    if (strcmp(user.name, name) == 0)
    {
      if (strcmp(user.database, database) == 0)
        return 1;
    }
    if (feof(file))
      break;
  }
  fclose(file);
  return 0;
}
```
## C. Data Definition Language

## D. Data Manipulation Language

## E. Logging
Logging diatur oleh fungsi `writeLog` dimana fungsi ini akan mengambil waktu
saati ini dan mencatat perintah pada file `logUser.log`

fungsi `writeLog`:  
```
void writelog(char *cmd, char *name)
{
  time_t times;
  struct tm *info;
  time(&times);
  info = localtime(&times);

  char info_log[1000];

  FILE *file;
  file = fopen("logUser.log", "ab");

  sprintf(info_log, "%d-%.2d-%.2d %.2d:%.2d:%.2d::%s::%s\n", info->tm_year + 1900, info->tm_mon + 1, info->tm_mday, info->tm_hour, info->tm_min, info->tm_sec, name, cmd);

  fputs(info_log, file);
  fclose(file);
  return;
}
```

## F. Reliability
Untuk program dump ada di `client_dump.c`

## G. Tambahan


## H. Error Handling
Error handling diatur pada fungsi `main()` di `database.c`. Nantinya
akan dilakukan percabangan untuk memeriksa perintah yang masuk dan
akan memeriksa adanya error seperti kesalahan input dan juga memeriksa 
apakah seorang user memiliki permission untuk database tersebut. Apabila
ada yang tidak sesuai maka akan dikeluarkan error.

fungsi `main` di `database.c`:  
```
...
    if (strcmp(cmd[0], "cUser") == 0)
    {
      if (strcmp(cmd[3], "0") == 0)
        createUser(cmd[1], cmd[2]);
      else
      {
        char warning[] = "Not Allowed: Not have permission";
        send(new_socket, warning, strlen(warning), 0);
        bzero(buff, sizeof(buff));
      }
    }
    else if (strcmp(cmd[0], "gPermission") == 0)
    {
      if (strcmp(cmd[3], "0") == 0)
      {
        int exist = isUserExist(cmd[2]);
        if (exist == 1)
          insertPermission(cmd[2], cmd[1]);
        else
        {
          char warning[] = "User Not Found";
          send(new_socket, warning, strlen(warning), 0);
          bzero(buff, sizeof(buff));
        }
      }
      else
      {
        char warning[] = "Not Allowed: Not have permission";
        send(new_socket, warning, strlen(warning), 0);
        bzero(buff, sizeof(buff));
      }
    }
    else if (strcmp(cmd[0], "cDatabase") == 0)
    {
      char loc[20000];
      snprintf(loc, sizeof loc, "databases/%s", cmd[1]);
      printf("location = %s, name = %s , database = %s\n", loc, cmd[2], cmd[1]);
      mkdir(loc, 0777);
      insertPermission(cmd[2], cmd[1]);
    }
    else if (strcmp(cmd[0], "uDatabase") == 0)
    {
      if (strcmp(cmd[3], "0") != 0)
      {
        int allowed = DBPermission(cmd[2], cmd[1]);
        if (allowed != 1)
        {
          char warning[] = "Access_database : You're Not Allowed";
          send(new_socket, warning, strlen(warning), 0);
          bzero(buff, sizeof(buff));
        }
        else
        {
          strncpy(used_db, cmd[1], sizeof(cmd[1]));
          char warning[] = "Access_database : Allowed";
          printf("used_db = %s\n", used_db);
          send(new_socket, warning, strlen(warning), 0);
          bzero(buff, sizeof(buff));
        }
      }
    }
    else if (strcmp(cmd[0], "cekCurrentDatabase") == 0)
    {
      if (used_db[0] == '\0')
      {
        strcpy(used_db, "You're not selecting database yet");
      }
      send(new_socket, used_db, strlen(used_db), 0);
      bzero(buff, sizeof(buff));
    }
    else if (strcmp(cmd[0], "cTable") == 0)
    {
      printf("%s\n", cmd[1]);
      char *toks;

      if (used_db[0] == '\0')
      {
        strcpy(used_db, "You're not selecting database yet");
        send(new_socket, used_db, strlen(used_db), 0);
        bzero(buff, sizeof(buff));
      }
      else
      {
        char query_list[100][10000];
        char temp_cmd[20000];
        snprintf(temp_cmd, sizeof temp_cmd, "%s", cmd[1]);
        toks = strtok(temp_cmd, "(), ");
        int total = 0;

        while (toks != NULL)
        {
          strcpy(query_list[total], toks);
          printf("%s\n", query_list[total]);
          total++;
          toks = strtok(NULL, "(), ");
        }

        char create_table[20000];
        snprintf(create_table, sizeof create_table, "../database/databases/%s/%s", used_db, query_list[2]);
        int iteration = 0;
        int data_iteration = 3;
        struct table column;

        while (total > 3)
        {
          strcpy(column.data[iteration], query_list[data_iteration]);
          printf("%s\n", column.data[iteration]);
          strcpy(column.type[iteration], query_list[data_iteration + 1]);
          data_iteration = data_iteration + 2;
          total = total - 2;
          iteration++;
        }

        column.tot_column = iteration;
        printf("iteration = %d\n", iteration);
        FILE *file;
        printf("%s\n", create_table);
        file = fopen(create_table, "ab");
        fwrite(&column, sizeof(column), 1, file);
        fclose(file);
      }
    }
    else if (strcmp(cmd[0], "dDatabase") == 0)
    {
      int allowed = DBPermission(cmd[2], cmd[1]);

      if (allowed != 1)
      {
        char warning[] = "Access_database : Not Allowed, no permission";
        send(new_socket, warning, strlen(warning), 0);
        bzero(buff, sizeof(buff));
        continue;
      }
      else
      {
        char delete[20000];
        snprintf(delete, sizeof delete, "rm -r databases/%s", cmd[1]);
        system(delete);
        char warning[] = "Database Has Been Removed";
        send(new_socket, warning, strlen(warning), 0);
        bzero(buff, sizeof(buff));
      }
    }
    else if (strcmp(cmd[0], "dTable") == 0)
    {
      if (used_db[0] == '\0')
      {
        strcpy(used_db, "You're not selecting database yet");
        send(new_socket, used_db, strlen(used_db), 0);
        bzero(buff, sizeof(buff));
        continue;
      }

      char delete[20000];
      snprintf(delete, sizeof delete, "databases/%s/%s", used_db, cmd[1]);
      remove(delete);
      char warning[] = "Table Has Been Removed";
      send(new_socket, warning, strlen(warning), 0);
      bzero(buff, sizeof(buff));
    }
    else if (strcmp(cmd[0], "dColumn") == 0)
    {
      if (used_db[0] == '\0')
      {
        strcpy(used_db, "You're not selecting database yet");
        send(new_socket, used_db, strlen(used_db), 0);
        bzero(buff, sizeof(buff));
        continue;
      }

      char create_table[20000];
      snprintf(create_table, sizeof create_table, "databases/%s/%s", used_db, cmd[2]);
      int index = findColumn(create_table, cmd[1]);

      if (index == -1)
      {
        char warning[] = "Column Not Found";
        send(new_socket, warning, strlen(warning), 0);
        bzero(buff, sizeof(buff));
        continue;
      }

      deleteColumn(create_table, index);
      char warning[] = "Column Has Been Removed";
      send(new_socket, warning, strlen(warning), 0);
      bzero(buff, sizeof(buff));
    }
    else if (strcmp(cmd[0], "insert") == 0)
    {
      if (used_db[0] == '\0')
      {
        strcpy(used_db, "You're not selecting database yet");
        send(new_socket, used_db, strlen(used_db), 0);
        bzero(buff, sizeof(buff));
        continue;
      }

      char query_list[100][10000];
      char temp_cmd[20000];
      snprintf(temp_cmd, sizeof temp_cmd, "%s", cmd[1]);
      char *toks;
      toks = strtok(temp_cmd, "\'(), ");
      int total = 0;

      while (toks != NULL)
      {
        strcpy(query_list[total], toks);
        total++;
        toks = strtok(NULL, "\'(), ");
      }

      char create_table[20000];
      snprintf(create_table, sizeof create_table, "databases/%s/%s", used_db, query_list[2]);
      FILE *file;
      int total_column;
      file = fopen(create_table, "r");

      if (file == NULL)
      {
        char warning[] = "TABLE NOT FOUND";
        send(new_socket, warning, strlen(warning), 0);
        bzero(buff, sizeof(buff));
        continue;
      }
      else
      {
        struct table user;
        fread(&user, sizeof(user), 1, file);
        total_column = user.tot_column;
        fclose(file);
      }

      int iteration = 0;
      int data_iteration = 3;
      struct table column;

      while (total > 3)
      {
        strcpy(column.data[iteration], query_list[data_iteration]);
        printf("%s\n", column.data[iteration]);
        strcpy(column.type[iteration], "string");
        data_iteration++;
        total = total - 1;
        iteration++;
      }

      column.tot_column = iteration;
      if (total_column != column.tot_column)
      {
        char warning[] = "YOUR INPUT NOT MATCH THE COLUMN";
        send(new_socket, warning, strlen(warning), 0);
        bzero(buff, sizeof(buff));
        continue;
      }

      printf("iteration = %d\n", iteration);
      FILE *file1;
      printf("%s\n", create_table);
      file1 = fopen(create_table, "ab");
      fwrite(&column, sizeof(column), 1, file1);
      fclose(file1);
      char warning[] = "Data Has Been Inserted";
      send(new_socket, warning, strlen(warning), 0);
      bzero(buff, sizeof(buff));
    }
    else if (strcmp(cmd[0], "update") == 0)
    {
      if (used_db[0] == '\0')
      {
        strcpy(used_db, "You're not selecting database yet");
        send(new_socket, used_db, strlen(used_db), 0);
        bzero(buff, sizeof(buff));
        continue;
      }
      char query_list[100][10000];
      char temp_cmd[20000];
      snprintf(temp_cmd, sizeof temp_cmd, "%s", cmd[1]);
      char *toks;
      toks = strtok(temp_cmd, "\'(),= ");
      int total = 0;
      while (toks != NULL)
      {
        strcpy(query_list[total], toks);
        printf("%s\n", query_list[total]);
        total++;
        toks = strtok(NULL, "\'(),= ");
      }
      printf("total = %d\n", total);
      char create_table[20000];
      snprintf(create_table, sizeof create_table, "databases/%s/%s", used_db, query_list[1]);
      if (total == 5)
      {
        printf("buat table = %s, kolumn = %s", create_table, query_list[3]);
        int index = findColumn(create_table, query_list[3]);
        if (index == -1)
        {
          char warning[] = "Column Not Found";
          send(new_socket, warning, strlen(warning), 0);
          bzero(buff, sizeof(buff));
          continue;
        }
        printf("index = %d\n", index);
        updateColumn(create_table, index, query_list[4]);
      }
      else if (total == 8)
      {
        printf("buat table = %s, kolumn = %s", create_table, query_list[3]);
        int index = findColumn(create_table, query_list[3]);
        if (index == -1)
        {
          char warning[] = "Column Not Found";
          send(new_socket, warning, strlen(warning), 0);
          bzero(buff, sizeof(buff));
          continue;
        }
        printf("%s\n", query_list[7]);
        int change_index = findColumn(create_table, query_list[6]);
        updateColumnWhere(create_table, index, query_list[4], change_index, query_list[7]);
      }
      else
      {
        char warning[] = "Data Has Been Deleted";
        send(new_socket, warning, strlen(warning), 0);
        bzero(buff, sizeof(buff));
        continue;
      }
      char warning[] = "Data Has Been Updated";
      send(new_socket, warning, strlen(warning), 0);
      bzero(buff, sizeof(buff));
    }
    else if (strcmp(cmd[0], "delete") == 0)
    {
      if (used_db[0] == '\0')
      {
        strcpy(used_db, "You're not selecting database yet");
        send(new_socket, used_db, strlen(used_db), 0);
        bzero(buff, sizeof(buff));
        continue;
      }
      char query_list[100][10000];
      char temp_cmd[20000];
      snprintf(temp_cmd, sizeof temp_cmd, "%s", cmd[1]);
      char *toks;
      toks = strtok(temp_cmd, "\'(),= ");
      int total = 0;
      while (toks != NULL)
      {
        strcpy(query_list[total], toks);
        printf("%s\n", query_list[total]);
        total++;
        toks = strtok(NULL, "\'(),= ");
      }
      printf("total = %d\n", total);
      char create_table[20000];
      snprintf(create_table, sizeof create_table, "databases/%s/%s", used_db, query_list[2]);
      if (total == 3)
      {
        deleteTable(create_table, query_list[2]);
      }
      else if (total == 6)
      {
        int index = findColumn(create_table, query_list[4]);
        if (index == -1)
        {
          char warning[] = "Column Not Found";
          send(new_socket, warning, strlen(warning), 0);
          bzero(buff, sizeof(buff));
          continue;
        }
        printf("index  = %d\n", index);
        deleteTableWhere(create_table, index, query_list[4], query_list[5]);
      }
      else
      {
        char warning[] = "Wrong input";
        send(new_socket, warning, strlen(warning), 0);
        bzero(buff, sizeof(buff));
        continue;
      }
      char warning[] = "Data Has Been Deleted";
      send(new_socket, warning, strlen(warning), 0);
      bzero(buff, sizeof(buff));
    }
    else if (strcmp(cmd[0], "select") == 0)
    {
      if (used_db[0] == '\0')
      {
        strcpy(used_db, "You're not selecting database yet");
        send(new_socket, used_db, strlen(used_db), 0);
        bzero(buff, sizeof(buff));
        continue;
      }
      char query_list[100][10000];
      char temp_cmd[20000];
      snprintf(temp_cmd, sizeof temp_cmd, "%s", cmd[1]);
      char *toks;
      toks = strtok(temp_cmd, "\'(),= ");
      int total = 0;
      while (toks != NULL)
      {
        strcpy(query_list[total], toks);
        printf("%s\n", query_list[total]);
        total++;
        toks = strtok(NULL, "\'(),= ");
      }
      printf("ABC\n");
      if (total == 4)
      {
        char create_table[20000];
        snprintf(create_table, sizeof create_table, "databases/%s/%s", used_db, query_list[3]);
        printf("buat table = %s", create_table);
        char perintahKolom[1000];
        printf("masuk 4\n");
        if (strcmp(query_list[1], "*") == 0)
        {
          FILE *file, *file1;
          struct table user;
          int id, mark = 0;
          file = fopen(create_table, "rb");
          char buffers[40000];
          char sendDatabase[40000];
          bzero(buff, sizeof(buff));
          bzero(sendDatabase, sizeof(sendDatabase));
          while (1)
          {
            char enter[] = "\n";
            fread(&user, sizeof(user), 1, file);
            snprintf(buffers, sizeof buffers, "\n");
            if (feof(file))
            {
              break;
            }
            for (int i = 0; i < user.tot_column; i++)
            {
              char padding[20000];
              snprintf(padding, sizeof padding, "%s\t", user.data[i]);
              strcat(buffers, padding);
            }
            strcat(sendDatabase, buffers);
          }
          send(new_socket, sendDatabase, strlen(sendDatabase), 0);
          bzero(sendDatabase, sizeof(sendDatabase));
          bzero(buff, sizeof(buff));
          fclose(file);
        }
        else
        {
          int index = findColumn(create_table, query_list[1]);
          printf("%d\n", index);
          FILE *file, *file1;
          struct table user;
          int id, mark = 0;
          file = fopen(create_table, "rb");
          char buffers[40000];
          char sendDatabase[40000];
          bzero(buff, sizeof(buff));
          bzero(sendDatabase, sizeof(sendDatabase));
          while (1)
          {
            char enter[] = "\n";
            fread(&user, sizeof(user), 1, file);
            snprintf(buffers, sizeof buffers, "\n");
            if (feof(file))
            {
              break;
            }
            for (int i = 0; i < user.tot_column; i++)
            {
              if (i == index)
              {
                char padding[20000];
                snprintf(padding, sizeof padding, "%s\t", user.data[i]);
                strcat(buffers, padding);
              }
            }
            strcat(sendDatabase, buffers);
          }
          printf("ini send fix\n%s\n", sendDatabase);
          fclose(file);
          send(new_socket, sendDatabase, strlen(sendDatabase), 0);
          bzero(sendDatabase, sizeof(sendDatabase));
          bzero(buff, sizeof(buff));
        }
      }
      else if (total == 7 && strcmp(query_list[4], "WHERE") == 0)
      {
        char create_table[20000];
        snprintf(create_table, sizeof create_table, "databases/%s/%s", used_db, query_list[3]);
        printf("buat table = %s", create_table);
        char perintahKolom[1000];
        printf("masuk 4\n");
        if (strcmp(query_list[1], "*") == 0)
        {
          FILE *file, *file1;
          struct table user;
          int id, mark = 0;
          file = fopen(create_table, "rb");
          char buffers[40000];
          char sendDatabase[40000];
          int index = findColumn(create_table, query_list[5]);
          printf("%d\n", index);
          bzero(buff, sizeof(buff));
          bzero(sendDatabase, sizeof(sendDatabase));
          while (1)
          {
            char enter[] = "\n";
            fread(&user, sizeof(user), 1, file);
            snprintf(buffers, sizeof buffers, "\n");
            if (feof(file))
            {
              break;
            }
            for (int i = 0; i < user.tot_column; i++)
            {
              if (strcmp(user.data[index], query_list[6]) == 0)
              {
                char padding[20000];
                snprintf(padding, sizeof padding, "%s\t", user.data[i]);
                strcat(buffers, padding);
              }
            }
            strcat(sendDatabase, buffers);
          }
          send(new_socket, sendDatabase, strlen(sendDatabase), 0);
          bzero(sendDatabase, sizeof(sendDatabase));
          bzero(buff, sizeof(buff));
          fclose(file);
        }
        else
        {
          int index = findColumn(create_table, query_list[1]);
          printf("%d\n", index);
          FILE *file, *file1;
          struct table user;
          int id, mark = 0;
          int change_index = findColumn(create_table, query_list[5]);
          file = fopen(create_table, "rb");
          char buffers[40000];
          char sendDatabase[40000];
          bzero(buff, sizeof(buff));
          bzero(sendDatabase, sizeof(sendDatabase));
          while (1)
          {
            char enter[] = "\n";
            fread(&user, sizeof(user), 1, file);
            snprintf(buffers, sizeof buffers, "\n");
            if (feof(file))
            {
              break;
            }
            for (int i = 0; i < user.tot_column; i++)
            {
              if (i == index && (strcmp(user.data[change_index], query_list[6]) == 0 || strcmp(user.data[i], query_list[5]) == 0))
              {
                char padding[20000];
                snprintf(padding, sizeof padding, "%s\t", user.data[i]);
                strcat(buffers, padding);
              }
            }
            strcat(sendDatabase, buffers);
          }
          printf("ini send fix\n%s\n", sendDatabase);
          fclose(file);
          send(new_socket, sendDatabase, strlen(sendDatabase), 0);
          bzero(sendDatabase, sizeof(sendDatabase));
          bzero(buff, sizeof(buff));
        }
      }
      else
      {
        printf("ini query 3 %s", query_list[total - 3]);
        if (strcmp(query_list[total - 3], "WHERE") != 0)
        {
          char create_table[20000];
          snprintf(create_table, sizeof create_table, "databases/%s/%s", used_db, query_list[total - 1]);
          printf("buat table = %s", create_table);
          printf("tanpa where");
          int index[100];
          int iteration = 0;
          for (int i = 1; i < total - 2; i++)
          {
            index[iteration] = findColumn(create_table, query_list[i]);
            printf("%d\n", index[iteration]);
            iteration++;
          }
        }
        else if (strcmp(query_list[total - 3], "WHERE") == 0)
        {
          printf("dengan where");
        }
      }
    }
    else if (strcmp(cmd[0], "log") == 0)
    {
      writelog(cmd[1], cmd[2]);
      char warning[] = "\n";
      send(new_socket, warning, strlen(warning), 0);
      bzero(buff, sizeof(buff));
    }
    if (strcmp(buff, ":exit") == 0)
    {
      printf("Disconnected from %s:%d\n", inet_ntoa(newAddr.sin_addr), ntohs(newAddr.sin_port));
      break;
    }
    else
    {
      printf("Client: %s\n", buff);
      send(new_socket, buff, strlen(buff), 0);
      bzero(buff, sizeof(buff));
    }
  }
}
}
```

## I. Containerization
Program akan dijalankan di dalam docker dengan spesifikasi sebagai berikut:

docker-compose.yaml  
```
version: '3'

services:
  storage-app:
    image: ngurahervan/fp
    deploy:
      replicas: 5
```

Dockerfile  
```
FROM ubuntu:latest

#update package
RUN apt-get update && apt-get install -y gcc

#copy database dan client ke container
COPY database/database.c .
COPY client/client.c .

#meng-compile database.c pada docker image
RUN gcc database.c -o database

#meng-compile database.c pada docker image
RUN gcc client.c -o client

#menjalankan kedua file
CMD sh -c './database & ./client'
```


## J. Extra
`client.c` dan `database.c` berkomunikasi dengan menggunakan socket
pada port 8080.

## Penjelasan kode
## client.c
`client.c` merupakan program yang mengatur sistem database dari sisi client.
Program mengatur autentikasi untuk user, membedakan antara user
root dan bukan root. Program ini mengambil input dari pengguna dan memisahkan 
kata-kata yang diinput yang akan diperiksa untuk menentukan perintah SQL yang 
tepat untuk dijalankan. Perintah tersebut dapat berupa operasi database seperti 
`CREATE`, `SELECT`, `UPDATE` dan `DELETE`. Setelah perintah tersebut dikirim 
ke program database.c, responnya akan ditampilkan ke pengguna. Program ini 
menggunakan socket untuk komunikasi antara kedua program. Seluruh akivitas
pengguna akan dicatat ke file log yang sesuai dengan pengguna
Berikut adalah Implementasi kami untuk program client:
```
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>
#include <time.h>

#define PORT 8080

struct acces {
  char name[10000];
  char pass[10000];
};

int autentikasi(char *username, char *password) {
  FILE *file;
  struct acces client;
  int id, found = 0;
  file = fopen("../database/databases/user.dat", "rb");

  while (fread(&client, sizeof(client), 1, file)) {
    if (strcmp(client.name, username) == 0 && strcmp(client.pass, password) == 0) {
      found = 1;
      break;
    }
  }

  fclose(file);

  if (found == 0) {
    printf("Not Allowed: No permission\n");
    return 0;
  } else {
    return 1;
  }
}

void HistoryLog(char *command, char *name) {
  time_t times;
  struct tm *info;
  time(&times);
  info = localtime(&times);

  char log_entry[1000];

  FILE *file;
  char loc[10000];
  snprintf(loc, sizeof(loc), "../database/log/log%s.log", name);
  file = fopen(loc, "ab");

  sprintf(log_entry, "%d-%.2d-%.2d %.2d:%.2d:%.2d:%s:%s;\n", info->tm_year + 1900, info->tm_mon + 1, info->tm_mday, info->tm_hour, info->tm_min, info->tm_sec, name, command);

  fputs(log_entry, file);
  fclose(file);
}

int main(int argc, char *argv[]) {
  int allowed = 0;
  int user_id = geteuid();
  char used_db[1000];

  if (geteuid() == 0)
    allowed = 1;
  else
    allowed = autentikasi(argv[2], argv[4]);

  if (allowed == 0)
    return 0;

  int client_socket, ret;
  struct sockaddr_in server_addr;
  char buffer[32000];

  client_socket = socket(AF_INET, SOCK_STREAM, 0);

  if (client_socket < 0) {
    printf("[-]Error creating socket.\n");
    exit(1);
  }

  printf("[+]Client Socket created.\n");

  memset(&server_addr, '\0', sizeof(server_addr));
  server_addr.sin_family = AF_INET;
  server_addr.sin_port = htons(PORT);
  server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");

  ret = connect(client_socket, (struct sockaddr *)&server_addr, sizeof(server_addr));

  if (ret < 0) {
    printf("[-]Error connecting.\n");
    exit(1);
  }
  printf("[+]Connected to the server.\n");

  while (1) {
    printf("Client: \t");
    char input[10000];
    char temp[10000];
    char cmd[100][10000];
    char *token;
    int i = 0;
    scanf(" %[^\n]s", input);
    strcpy(temp, input);
    token = strtok(input, " ");

    while (token != NULL) {
      strcpy(cmd[i], token);
      i++;
      token = strtok(NULL, " ");
    }

    int false_cmd = 0;
    //CREATE USER
    if (strcmp(cmd[0], "CREATE") == 0) {
      if (strcmp(cmd[1], "USER") == 0 && strcmp(cmd[3], "IDENTIFIED") == 0 && strcmp(cmd[4], "BY") == 0) {
        snprintf(buffer, sizeof(buffer), "cUser:%s:%s:%d", cmd[2], cmd[5], user_id);
        send(client_socket, buffer, strlen(buffer), 0);
      }
    //CREATE DATABASE
      else if (strcmp(cmd[1], "DATABASE") == 0) {
        snprintf(buffer, sizeof(buffer), "cDatabase:%s:%s:%d", cmd[2], argv[2], user_id);
        send(client_socket, buffer, strlen(buffer), 0);
      }
    //CREATE TABLE
      else if (strcmp(cmd[1], "TABLE") == 0) {
        snprintf(buffer, sizeof(buffer), "cTable:%s", temp);
        send(client_socket, buffer, strlen(buffer), 0);
      }
    }
    //GRANT PERMISSION
    else if (strcmp(cmd[0], "GRANT") == 0 && strcmp(cmd[1], "PERMISSION") == 0 && strcmp(cmd[3], "INTO") == 0) {
      snprintf(buffer, sizeof(buffer), "gPermission:%s:%s:%d", cmd[2], cmd[4], user_id);
      send(client_socket, buffer, strlen(buffer), 0);
    }
    // USE
    else if (strcmp(cmd[0], "USE") == 0) {
      snprintf(buffer, sizeof(buffer), "uDatabase:%s:%s:%d", cmd[1], argv[2], user_id);
      send(client_socket, buffer, strlen(buffer), 0);
    }
    // cekCurrentDatabase
    else if (strcmp(cmd[0], "cekCurrentDatabase") == 0) {
      snprintf(buffer, sizeof(buffer), "%s", cmd[0]);
      send(client_socket, buffer, strlen(buffer), 0);
    }
    //DROP 
    else if (strcmp(cmd[0], "DROP") == 0) {
      if (strcmp(cmd[1], "DATABASE") == 0) {
        snprintf(buffer, sizeof(buffer), "dDatabase:%s:%s", cmd[2], argv[2]);
        send(client_socket, buffer, strlen(buffer), 0);
      }
      else if (strcmp(cmd[1], "TABLE") == 0) {
        snprintf(buffer, sizeof(buffer), "dTable:%s:%s", cmd[2], argv[2]);
        send(client_socket, buffer, strlen(buffer), 0);
      }
      else if (strcmp(cmd[1], "COLUMN") == 0) {
        snprintf(buffer, sizeof(buffer), "dColumn:%s:%s:%s", cmd[2], cmd[4], argv[2]);
        send(client_socket, buffer, strlen(buffer), 0);
      }
    }
    //INSERT
    else if (strcmp(cmd[0], "INSERT") == 0 && strcmp(cmd[1], "INTO") == 0) {
      snprintf(buffer, sizeof(buffer), "insert:%s", temp);
      send(client_socket, buffer, strlen(buffer), 0);
    }
    //UPDATE
    else if (strcmp(cmd[0], "UPDATE") == 0) {
      snprintf(buffer, sizeof(buffer), "update:%s", temp);
      send(client_socket, buffer, strlen(buffer), 0);
    }
    // DELETE
    else if (strcmp(cmd[0], "DELETE") == 0) {
      snprintf(buffer, sizeof(buffer), "delete:%s", temp);
      send(client_socket, buffer, strlen(buffer), 0);
    }
    // SELECT
    else if (strcmp(cmd[0], "SELECT") == 0) {
      snprintf(buffer, sizeof(buffer), "select:%s", temp);
      send(client_socket, buffer, strlen(buffer), 0);
    }
    //EXIT
    else if (strcmp(cmd[0], ":exit") != 0) {
      false_cmd = 1;
      char warning[] = "Invalid Command";
      send(client_socket, warning, strlen(warning), 0);
    }

    if (false_cmd != 1) {
      char sender[10000];
      if (user_id == 0)
        strcpy(sender, "root");
      else
        strcpy(sender, argv[2]);
      HistoryLog(temp, sender);
    }

    if (strcmp(cmd[0], ":exit") == 0) {
      send(client_socket, cmd[0], strlen(cmd[0]), 0);
      close(client_socket);
      printf("[-]Disconnected from server.\n");
      exit(1);
    }
    bzero(buffer, sizeof(buffer));
    if (recv(client_socket, buffer, 1024, 0) < 0)
      printf("[-]Error receiving data.\n");
    else
      printf("Server: \t%s\n", buffer);
  }

  return 0;
}
```
Penjelasan fungsi:  

- Fungsi `autentikasi()` digunakan untuk melakukan autentikasi pengguna 
dengan memeriksa username dan password yang diberikan. Bekerja dengan cara 
membuka file "user.dat" yang berisi informasi nama pengguna dan kata sandi 
yang tersimpan. Lalu nama pengguna dan kata sandi yang diberikan dibandingkan
dengan entri yang ada di file. Jika ada kecocokan, pengguna dianggap berhasil 
diautentikasi, dan fungsi ini mengembalikan nilai 1. Jika tidak ada kecocokan,
fungsi ini mengembalikan nilai 0.
- Fungsi `HistoryLog()` digunakan untuk mencatat log aktivitas pengguna. 
Pertama-tama program mendapatkan waktu sistem saat ini. Dilanjutkan dengan 
membuat string log_entry yang berisi informasi waktu, nama pengguna, dan 
perintah yang dilakukan. Lalu program membuka file log pengguna yang sesuai 
berdasarkan nama pengguna dan menambahkan log_entry ke file. Terakhir file 
log ditutup.
- Fungsi `main()` mengecek izin pengguna atau melakukan autentikasi 
jika pengguna tidak memiliki izin root. Socket client dibuat dan dihubungkan 
dengan server menggunakan alamat IP dan port tertentu. Lalu terdapat loop 
yang terus berjalan hingga pengguna memasukkan perintah ":exit" untuk keluar.
Dalam loop tersebut program menerima perintah SQL dari pengguna dan 
memprosesnya sesuai dengan jenis perintah yang diberikan. Perintah tersebut 
kemudian akan dikirim ke database melalui socket dan menerima respons dari 
server. Jika perintah valid, maka log aktivitas pengguna akan dicatat. Dan 
jika perintah ":exit" diberikan, koneksi ditutup dan program berakhir.

## database.c
`database.c` merupakan program yang mengatur sistem database dari sisi server.
Program mengambil input dari client dan memutuskan apa yang akan dilakukan
untuk database tersebut. Untuk melaksanakan suatu perintah SQL dalam database
ini, pengguna dapat melakukannya dari sisi client. Apabila perintah yang
diberikan salah atau tidak dapat dijalankan, maka server mengirimkan error
yang dapat membantu pengguna untuk menyelesaikan errornya. `database.c` juga
mengatur permission untuk database sehingga hanya user yang diberikan akses
database oleh root yang dapat mengakses database.

Berikut adalah Implementasi kami untuk program database:
```
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>

#define PORT 8080

struct table
{
  int tot_column;
  char type[100][10000];
  char data[100][10000];
};

struct permission
{
  char name[10000];
  char pass[10000];
};

struct permission_db
{
  char database[10000];
  char name[10000];
};

int isUserExist(char *uname)
{
  FILE *file;
  struct permission user;

  int id, mark = 0;

  file = fopen("../database/databases/user.dat", "rb");
  while (1)
  {
    fread(&user, sizeof(user), 1, file);
    if (strcmp(user.name, uname) == 0)
      return 1;
    if (feof(file))
      break;
  }
  fclose(file);
  return 0;
}

void createUser(char *name, char *pass)
{
  struct permission user;
  strcpy(user.name, name);
  strcpy(user.pass, pass);

  printf("%s %s\n", user.name, user.pass);

  char fname[] = {"databases/user.dat"};

  FILE *file;
  file = fopen(fname, "ab");
  fwrite(&user, sizeof(user), 1, file);
  fclose(file);
}

int DBPermission(char *name, char *database)
{
  FILE *file;
  struct permission_db user;
  int id, mark = 0;
  printf("name = %s  database = %s", name, database);
  file = fopen("../database/databases/permission.dat", "rb");
  while (1)
  {
    fread(&user, sizeof(user), 1, file);
    if (strcmp(user.name, name) == 0)
    {
      if (strcmp(user.database, database) == 0)
        return 1;
    }
    if (feof(file))
      break;
  }
  fclose(file);
  return 0;
}

void insertPermission(char *name, char *database)
{
  struct permission_db user;

  strcpy(user.name, name);
  strcpy(user.database, database);

  printf("%s %s\n", user.name, user.database);

  char fname[] = {"databases/permission.dat"};
  FILE *file;
  file = fopen(fname, "ab");
  fwrite(&user, sizeof(user), 1, file);
  fclose(file);
}

int findColumn(char *table, char *column)
{
  FILE *file;
  struct table user;
  int id, mark = 0;
  file = fopen(table, "rb");
  fread(&user, sizeof(user), 1, file);
  int index = -1;

  for (int i = 0; i < user.tot_column; i++)
  {
    if (strcmp(user.data[i], column) == 0)
      index = i;
  }

  if (feof(file))
    return -1;

  fclose(file);
  return index;
}

int deleteColumn(char *table, int index)
{
  FILE *file, *file1;
  struct table user;
  int id, mark = 0;
  file = fopen(table, "rb");
  file1 = fopen("temp", "wb");

  while (1)
  {
    fread(&user, sizeof(user), 1, file);

    if (feof(file))
      break;

    struct table temp_user;
    int iteration = 0;

    for (int i = 0; i < user.tot_column; i++)
    {
      if (i == index)
        continue;

      strcpy(temp_user.data[iteration], user.data[i]);
      strcpy(temp_user.type[iteration], user.type[i]);
      printf("%s\n", temp_user.data[iteration]);
      iteration++;
    }
    temp_user.tot_column = user.tot_column - 1;
    fwrite(&temp_user, sizeof(temp_user), 1, file1);
  }

  fclose(file);
  fclose(file1);
  remove(table);
  rename("temp", table);
  return 0;
}

int deleteTable(char *table, char *namaTable)
{
  FILE *file, *file1;
  struct table user;
  int id, mark = 0;
  file = fopen(table, "rb");
  file1 = fopen("temp", "ab");
  fread(&user, sizeof(user), 1, file);
  int index = -1;
  struct table temp_user;

  for (int i = 0; i < user.tot_column; i++)
  {
    strcpy(temp_user.data[i], user.data[i]);
    strcpy(temp_user.type[i], user.type[i]);
  }

  temp_user.tot_column = user.tot_column;
  fwrite(&temp_user, sizeof(temp_user), 1, file1);
  fclose(file);
  fclose(file1);
  remove(table);
  rename("temp", table);
  return 1;
}

int updateColumn(char *table, int index, char *ganti)
{
  FILE *file, *file1;
  struct table user;
  int id, mark = 0;
  file = fopen(table, "rb");
  file1 = fopen("temp", "ab");
  int data_take = 0;

  while (1)
  {
    fread(&user, sizeof(user), 1, file);
    if (feof(file))
      break;

    struct table temp_user;
    int iteration = 0;
    for (int i = 0; i < user.tot_column; i++)
    {
      if (i == index && data_take != 0)
        strcpy(temp_user.data[iteration], ganti);
      else
        strcpy(temp_user.data[iteration], user.data[i]);

      printf("%s\n", temp_user.data[iteration]);
      strcpy(temp_user.type[iteration], user.type[i]);
      printf("%s\n", temp_user.data[iteration]);
      iteration++;
    }
    temp_user.tot_column = user.tot_column;
    fwrite(&temp_user, sizeof(temp_user), 1, file1);
    data_take++;
  }
  fclose(file);
  fclose(file1);
  remove(table);
  rename("temp", table);
  return 0;
}

int updateColumnWhere(char *table, int index, char *ganti, int change_index, char *where)
{
  FILE *file, *file1;
  struct table user;
  int id, mark = 0;
  file = fopen(table, "rb");
  file1 = fopen("temp", "ab");
  int data_take = 0;

  while (1)
  {
    fread(&user, sizeof(user), 1, file);
    if (feof(file))
      break;

    struct table temp_user;
    int iteration = 0;

    for (int i = 0; i < user.tot_column; i++)
    {
      if (i == index && data_take != 0 && strcmp(user.data[change_index], where) == 0)
        strcpy(temp_user.data[iteration], ganti);
      else
        strcpy(temp_user.data[iteration], user.data[i]);
      printf("%s\n", temp_user.data[iteration]);
      strcpy(temp_user.type[iteration], user.type[i]);
      printf("%s\n", temp_user.data[iteration]);
      iteration++;
    }

    temp_user.tot_column = user.tot_column;
    fwrite(&temp_user, sizeof(temp_user), 1, file1);
    data_take++;
  }
  fclose(file);
  fclose(file1);
  remove(table);
  rename("temp", table);
  return 0;
}

int deleteTableWhere(char *table, int index, char *column, char *where)
{
  FILE *file, *file1;
  struct table user;
  int id, mark = 0;
  file = fopen(table, "rb");
  file1 = fopen("temp", "ab");
  int data_take = 0;

  while (1)
  {
    mark = 0;
    fread(&user, sizeof(user), 1, file);

    if (feof(file))
      break;

    struct table temp_user;
    int iteration = 0;

    for (int i = 0; i < user.tot_column; i++)
    {
      if (i == index && data_take != 0 && strcmp(user.data[i], where) == 0)
        mark = 1;
      strcpy(temp_user.data[iteration], user.data[i]);
      printf("%s\n", temp_user.data[iteration]);
      strcpy(temp_user.type[iteration], user.type[i]);
      printf("%s\n", temp_user.data[iteration]);
      iteration++;
    }

    temp_user.tot_column = user.tot_column;
    if (mark != 1)
      fwrite(&temp_user, sizeof(temp_user), 1, file1);
    data_take++;
  }
  fclose(file);
  fclose(file1);
  remove(table);
  rename("temp", table);
  return 0;
}

void writelog(char *cmd, char *name)
{
  time_t times;
  struct tm *info;
  time(&times);
  info = localtime(&times);

  char info_log[1000];

  FILE *file;
  file = fopen("logUser.log", "ab");

  sprintf(info_log, "%d-%.2d-%.2d %.2d:%.2d:%.2d::%s::%s\n", info->tm_year + 1900, info->tm_mon + 1, info->tm_mday, info->tm_hour, info->tm_min, info->tm_sec, name, cmd);

  fputs(info_log, file);
  fclose(file);
  return;
}

int main()
{

  int sockfd, ret;
  struct sockaddr_in serverAddr;

  int new_socket;
  struct sockaddr_in newAddr;

  socklen_t addr_size;

  char buff[1024];
  pid_t childpid;

  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0)
  {
    printf("[-]Error connection.\n");
    exit(1);
  }
  printf("[+]Socket created.\n");

  memset(&serverAddr, '\0', sizeof(serverAddr));
  serverAddr.sin_family = AF_INET;
  serverAddr.sin_port = htons(PORT);
  serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

  ret = bind(sockfd, (struct sockaddr *)&serverAddr, sizeof(serverAddr));
  if (ret < 0)
  {
    printf("[-]Error in binding.\n");
    exit(1);
  }
  printf("[+]Bind to port %d\n", PORT);

  if (listen(sockfd, 10) == 0)
    printf("[+]Listening....\n");
  else
    printf("[-]Error in binding.\n");

  while (1)
  {
    new_socket = accept(sockfd, (struct sockaddr *)&newAddr, &addr_size);

    if (new_socket < 0)
      exit(1);
    printf("Connection accepted from %s:%d\n", inet_ntoa(newAddr.sin_addr), ntohs(newAddr.sin_port));

    if ((childpid = fork()) == 0)
    {
      close(sockfd);

      while (1)
      {
        recv(new_socket, buff, 1024, 0);
        char *token;
        char temp_buff[32000];
        strcpy(temp_buff, buff);
        char cmd[100][10000];
        token = strtok(temp_buff, ":");
        int i = 0;
        char used_db[1000];

        while (token != NULL)
        {
          strcpy(cmd[i], token);
          i++;
          token = strtok(NULL, ":");
        }
        if (strcmp(cmd[0], "cUser") == 0)
        {
          if (strcmp(cmd[3], "0") == 0)
            createUser(cmd[1], cmd[2]);
          else
          {
            char warning[] = "Not Allowed: Not have permission";
            send(new_socket, warning, strlen(warning), 0);
            bzero(buff, sizeof(buff));
          }
        }
        else if (strcmp(cmd[0], "gPermission") == 0)
        {
          if (strcmp(cmd[3], "0") == 0)
          {
            int exist = isUserExist(cmd[2]);
            if (exist == 1)
              insertPermission(cmd[2], cmd[1]);
            else
            {
              char warning[] = "User Not Found";
              send(new_socket, warning, strlen(warning), 0);
              bzero(buff, sizeof(buff));
            }
          }
          else
          {
            char warning[] = "Not Allowed: Not have permission";
            send(new_socket, warning, strlen(warning), 0);
            bzero(buff, sizeof(buff));
          }
        }
        else if (strcmp(cmd[0], "cDatabase") == 0)
        {
          char loc[20000];
          snprintf(loc, sizeof loc, "databases/%s", cmd[1]);
          printf("location = %s, name = %s , database = %s\n", loc, cmd[2], cmd[1]);
          mkdir(loc, 0777);
          insertPermission(cmd[2], cmd[1]);
        }
        else if (strcmp(cmd[0], "uDatabase") == 0)
        {
          if (strcmp(cmd[3], "0") != 0)
          {
            int allowed = DBPermission(cmd[2], cmd[1]);
            if (allowed != 1)
            {
              char warning[] = "Access_database : You're Not Allowed";
              send(new_socket, warning, strlen(warning), 0);
              bzero(buff, sizeof(buff));
            }
            else
            {
              strncpy(used_db, cmd[1], sizeof(cmd[1]));
              char warning[] = "Access_database : Allowed";
              printf("used_db = %s\n", used_db);
              send(new_socket, warning, strlen(warning), 0);
              bzero(buff, sizeof(buff));
            }
          }
        }
        else if (strcmp(cmd[0], "cekCurrentDatabase") == 0)
        {
          if (used_db[0] == '\0')
          {
            strcpy(used_db, "You're not selecting database yet");
          }
          send(new_socket, used_db, strlen(used_db), 0);
          bzero(buff, sizeof(buff));
        }
        else if (strcmp(cmd[0], "cTable") == 0)
        {
          printf("%s\n", cmd[1]);
          char *toks;

          if (used_db[0] == '\0')
          {
            strcpy(used_db, "You're not selecting database yet");
            send(new_socket, used_db, strlen(used_db), 0);
            bzero(buff, sizeof(buff));
          }
          else
          {
            char query_list[100][10000];
            char temp_cmd[20000];
            snprintf(temp_cmd, sizeof temp_cmd, "%s", cmd[1]);
            toks = strtok(temp_cmd, "(), ");
            int total = 0;

            while (toks != NULL)
            {
              strcpy(query_list[total], toks);
              printf("%s\n", query_list[total]);
              total++;
              toks = strtok(NULL, "(), ");
            }

            char create_table[20000];
            snprintf(create_table, sizeof create_table, "../database/databases/%s/%s", used_db, query_list[2]);
            int iteration = 0;
            int data_iteration = 3;
            struct table column;

            while (total > 3)
            {
              strcpy(column.data[iteration], query_list[data_iteration]);
              printf("%s\n", column.data[iteration]);
              strcpy(column.type[iteration], query_list[data_iteration + 1]);
              data_iteration = data_iteration + 2;
              total = total - 2;
              iteration++;
            }

            column.tot_column = iteration;
            printf("iteration = %d\n", iteration);
            FILE *file;
            printf("%s\n", create_table);
            file = fopen(create_table, "ab");
            fwrite(&column, sizeof(column), 1, file);
            fclose(file);
          }
        }
        else if (strcmp(cmd[0], "dDatabase") == 0)
        {
          int allowed = DBPermission(cmd[2], cmd[1]);

          if (allowed != 1)
          {
            char warning[] = "Access_database : Not Allowed, no permission";
            send(new_socket, warning, strlen(warning), 0);
            bzero(buff, sizeof(buff));
            continue;
          }
          else
          {
            char delete[20000];
            snprintf(delete, sizeof delete, "rm -r databases/%s", cmd[1]);
            system(delete);
            char warning[] = "Database Has Been Removed";
            send(new_socket, warning, strlen(warning), 0);
            bzero(buff, sizeof(buff));
          }
        }
        else if (strcmp(cmd[0], "dTable") == 0)
        {
          if (used_db[0] == '\0')
          {
            strcpy(used_db, "You're not selecting database yet");
            send(new_socket, used_db, strlen(used_db), 0);
            bzero(buff, sizeof(buff));
            continue;
          }

          char delete[20000];
          snprintf(delete, sizeof delete, "databases/%s/%s", used_db, cmd[1]);
          remove(delete);
          char warning[] = "Table Has Been Removed";
          send(new_socket, warning, strlen(warning), 0);
          bzero(buff, sizeof(buff));
        }
        else if (strcmp(cmd[0], "dColumn") == 0)
        {
          if (used_db[0] == '\0')
          {
            strcpy(used_db, "You're not selecting database yet");
            send(new_socket, used_db, strlen(used_db), 0);
            bzero(buff, sizeof(buff));
            continue;
          }

          char create_table[20000];
          snprintf(create_table, sizeof create_table, "databases/%s/%s", used_db, cmd[2]);
          int index = findColumn(create_table, cmd[1]);

          if (index == -1)
          {
            char warning[] = "Column Not Found";
            send(new_socket, warning, strlen(warning), 0);
            bzero(buff, sizeof(buff));
            continue;
          }

          deleteColumn(create_table, index);
          char warning[] = "Column Has Been Removed";
          send(new_socket, warning, strlen(warning), 0);
          bzero(buff, sizeof(buff));
        }
        else if (strcmp(cmd[0], "insert") == 0)
        {
          if (used_db[0] == '\0')
          {
            strcpy(used_db, "You're not selecting database yet");
            send(new_socket, used_db, strlen(used_db), 0);
            bzero(buff, sizeof(buff));
            continue;
          }

          char query_list[100][10000];
          char temp_cmd[20000];
          snprintf(temp_cmd, sizeof temp_cmd, "%s", cmd[1]);
          char *toks;
          toks = strtok(temp_cmd, "\'(), ");
          int total = 0;

          while (toks != NULL)
          {
            strcpy(query_list[total], toks);
            total++;
            toks = strtok(NULL, "\'(), ");
          }

          char create_table[20000];
          snprintf(create_table, sizeof create_table, "databases/%s/%s", used_db, query_list[2]);
          FILE *file;
          int total_column;
          file = fopen(create_table, "r");

          if (file == NULL)
          {
            char warning[] = "TABLE NOT FOUND";
            send(new_socket, warning, strlen(warning), 0);
            bzero(buff, sizeof(buff));
            continue;
          }
          else
          {
            struct table user;
            fread(&user, sizeof(user), 1, file);
            total_column = user.tot_column;
            fclose(file);
          }

          int iteration = 0;
          int data_iteration = 3;
          struct table column;

          while (total > 3)
          {
            strcpy(column.data[iteration], query_list[data_iteration]);
            printf("%s\n", column.data[iteration]);
            strcpy(column.type[iteration], "string");
            data_iteration++;
            total = total - 1;
            iteration++;
          }

          column.tot_column = iteration;
          if (total_column != column.tot_column)
          {
            char warning[] = "YOUR INPUT NOT MATCH THE COLUMN";
            send(new_socket, warning, strlen(warning), 0);
            bzero(buff, sizeof(buff));
            continue;
          }

          printf("iteration = %d\n", iteration);
          FILE *file1;
          printf("%s\n", create_table);
          file1 = fopen(create_table, "ab");
          fwrite(&column, sizeof(column), 1, file1);
          fclose(file1);
          char warning[] = "Data Has Been Inserted";
          send(new_socket, warning, strlen(warning), 0);
          bzero(buff, sizeof(buff));
        }
        else if (strcmp(cmd[0], "update") == 0)
        {
          if (used_db[0] == '\0')
          {
            strcpy(used_db, "You're not selecting database yet");
            send(new_socket, used_db, strlen(used_db), 0);
            bzero(buff, sizeof(buff));
            continue;
          }
          char query_list[100][10000];
          char temp_cmd[20000];
          snprintf(temp_cmd, sizeof temp_cmd, "%s", cmd[1]);
          char *toks;
          toks = strtok(temp_cmd, "\'(),= ");
          int total = 0;
          while (toks != NULL)
          {
            strcpy(query_list[total], toks);
            printf("%s\n", query_list[total]);
            total++;
            toks = strtok(NULL, "\'(),= ");
          }
          printf("total = %d\n", total);
          char create_table[20000];
          snprintf(create_table, sizeof create_table, "databases/%s/%s", used_db, query_list[1]);
          if (total == 5)
          {
            printf("buat table = %s, kolumn = %s", create_table, query_list[3]);
            int index = findColumn(create_table, query_list[3]);
            if (index == -1)
            {
              char warning[] = "Column Not Found";
              send(new_socket, warning, strlen(warning), 0);
              bzero(buff, sizeof(buff));
              continue;
            }
            printf("index = %d\n", index);
            updateColumn(create_table, index, query_list[4]);
          }
          else if (total == 8)
          {
            printf("buat table = %s, kolumn = %s", create_table, query_list[3]);
            int index = findColumn(create_table, query_list[3]);
            if (index == -1)
            {
              char warning[] = "Column Not Found";
              send(new_socket, warning, strlen(warning), 0);
              bzero(buff, sizeof(buff));
              continue;
            }
            printf("%s\n", query_list[7]);
            int change_index = findColumn(create_table, query_list[6]);
            updateColumnWhere(create_table, index, query_list[4], change_index, query_list[7]);
          }
          else
          {
            char warning[] = "Data Has Been Deleted";
            send(new_socket, warning, strlen(warning), 0);
            bzero(buff, sizeof(buff));
            continue;
          }
          char warning[] = "Data Has Been Updated";
          send(new_socket, warning, strlen(warning), 0);
          bzero(buff, sizeof(buff));
        }
        else if (strcmp(cmd[0], "delete") == 0)
        {
          if (used_db[0] == '\0')
          {
            strcpy(used_db, "You're not selecting database yet");
            send(new_socket, used_db, strlen(used_db), 0);
            bzero(buff, sizeof(buff));
            continue;
          }
          char query_list[100][10000];
          char temp_cmd[20000];
          snprintf(temp_cmd, sizeof temp_cmd, "%s", cmd[1]);
          char *toks;
          toks = strtok(temp_cmd, "\'(),= ");
          int total = 0;
          while (toks != NULL)
          {
            strcpy(query_list[total], toks);
            printf("%s\n", query_list[total]);
            total++;
            toks = strtok(NULL, "\'(),= ");
          }
          printf("total = %d\n", total);
          char create_table[20000];
          snprintf(create_table, sizeof create_table, "databases/%s/%s", used_db, query_list[2]);
          if (total == 3)
          {
            deleteTable(create_table, query_list[2]);
          }
          else if (total == 6)
          {
            int index = findColumn(create_table, query_list[4]);
            if (index == -1)
            {
              char warning[] = "Column Not Found";
              send(new_socket, warning, strlen(warning), 0);
              bzero(buff, sizeof(buff));
              continue;
            }
            printf("index  = %d\n", index);
            deleteTableWhere(create_table, index, query_list[4], query_list[5]);
          }
          else
          {
            char warning[] = "Wrong input";
            send(new_socket, warning, strlen(warning), 0);
            bzero(buff, sizeof(buff));
            continue;
          }
          char warning[] = "Data Has Been Deleted";
          send(new_socket, warning, strlen(warning), 0);
          bzero(buff, sizeof(buff));
        }
        else if (strcmp(cmd[0], "select") == 0)
        {
          if (used_db[0] == '\0')
          {
            strcpy(used_db, "You're not selecting database yet");
            send(new_socket, used_db, strlen(used_db), 0);
            bzero(buff, sizeof(buff));
            continue;
          }
          char query_list[100][10000];
          char temp_cmd[20000];
          snprintf(temp_cmd, sizeof temp_cmd, "%s", cmd[1]);
          char *toks;
          toks = strtok(temp_cmd, "\'(),= ");
          int total = 0;
          while (toks != NULL)
          {
            strcpy(query_list[total], toks);
            printf("%s\n", query_list[total]);
            total++;
            toks = strtok(NULL, "\'(),= ");
          }
          printf("ABC\n");
          if (total == 4)
          {
            char create_table[20000];
            snprintf(create_table, sizeof create_table, "databases/%s/%s", used_db, query_list[3]);
            printf("buat table = %s", create_table);
            char perintahKolom[1000];
            printf("masuk 4\n");
            if (strcmp(query_list[1], "*") == 0)
            {
              FILE *file, *file1;
              struct table user;
              int id, mark = 0;
              file = fopen(create_table, "rb");
              char buffers[40000];
              char sendDatabase[40000];
              bzero(buff, sizeof(buff));
              bzero(sendDatabase, sizeof(sendDatabase));
              while (1)
              {
                char enter[] = "\n";
                fread(&user, sizeof(user), 1, file);
                snprintf(buffers, sizeof buffers, "\n");
                if (feof(file))
                {
                  break;
                }
                for (int i = 0; i < user.tot_column; i++)
                {
                  char padding[20000];
                  snprintf(padding, sizeof padding, "%s\t", user.data[i]);
                  strcat(buffers, padding);
                }
                strcat(sendDatabase, buffers);
              }
              send(new_socket, sendDatabase, strlen(sendDatabase), 0);
              bzero(sendDatabase, sizeof(sendDatabase));
              bzero(buff, sizeof(buff));
              fclose(file);
            }
            else
            {
              int index = findColumn(create_table, query_list[1]);
              printf("%d\n", index);
              FILE *file, *file1;
              struct table user;
              int id, mark = 0;
              file = fopen(create_table, "rb");
              char buffers[40000];
              char sendDatabase[40000];
              bzero(buff, sizeof(buff));
              bzero(sendDatabase, sizeof(sendDatabase));
              while (1)
              {
                char enter[] = "\n";
                fread(&user, sizeof(user), 1, file);
                snprintf(buffers, sizeof buffers, "\n");
                if (feof(file))
                {
                  break;
                }
                for (int i = 0; i < user.tot_column; i++)
                {
                  if (i == index)
                  {
                    char padding[20000];
                    snprintf(padding, sizeof padding, "%s\t", user.data[i]);
                    strcat(buffers, padding);
                  }
                }
                strcat(sendDatabase, buffers);
              }
              printf("ini send fix\n%s\n", sendDatabase);
              fclose(file);
              send(new_socket, sendDatabase, strlen(sendDatabase), 0);
              bzero(sendDatabase, sizeof(sendDatabase));
              bzero(buff, sizeof(buff));
            }
          }
          else if (total == 7 && strcmp(query_list[4], "WHERE") == 0)
          {
            char create_table[20000];
            snprintf(create_table, sizeof create_table, "databases/%s/%s", used_db, query_list[3]);
            printf("buat table = %s", create_table);
            char perintahKolom[1000];
            printf("masuk 4\n");
            if (strcmp(query_list[1], "*") == 0)
            {
              FILE *file, *file1;
              struct table user;
              int id, mark = 0;
              file = fopen(create_table, "rb");
              char buffers[40000];
              char sendDatabase[40000];
              int index = findColumn(create_table, query_list[5]);
              printf("%d\n", index);
              bzero(buff, sizeof(buff));
              bzero(sendDatabase, sizeof(sendDatabase));
              while (1)
              {
                char enter[] = "\n";
                fread(&user, sizeof(user), 1, file);
                snprintf(buffers, sizeof buffers, "\n");
                if (feof(file))
                {
                  break;
                }
                for (int i = 0; i < user.tot_column; i++)
                {
                  if (strcmp(user.data[index], query_list[6]) == 0)
                  {
                    char padding[20000];
                    snprintf(padding, sizeof padding, "%s\t", user.data[i]);
                    strcat(buffers, padding);
                  }
                }
                strcat(sendDatabase, buffers);
              }
              send(new_socket, sendDatabase, strlen(sendDatabase), 0);
              bzero(sendDatabase, sizeof(sendDatabase));
              bzero(buff, sizeof(buff));
              fclose(file);
            }
            else
            {
              int index = findColumn(create_table, query_list[1]);
              printf("%d\n", index);
              FILE *file, *file1;
              struct table user;
              int id, mark = 0;
              int change_index = findColumn(create_table, query_list[5]);
              file = fopen(create_table, "rb");
              char buffers[40000];
              char sendDatabase[40000];
              bzero(buff, sizeof(buff));
              bzero(sendDatabase, sizeof(sendDatabase));
              while (1)
              {
                char enter[] = "\n";
                fread(&user, sizeof(user), 1, file);
                snprintf(buffers, sizeof buffers, "\n");
                if (feof(file))
                {
                  break;
                }
                for (int i = 0; i < user.tot_column; i++)
                {
                  if (i == index && (strcmp(user.data[change_index], query_list[6]) == 0 || strcmp(user.data[i], query_list[5]) == 0))
                  {
                    char padding[20000];
                    snprintf(padding, sizeof padding, "%s\t", user.data[i]);
                    strcat(buffers, padding);
                  }
                }
                strcat(sendDatabase, buffers);
              }
              printf("ini send fix\n%s\n", sendDatabase);
              fclose(file);
              send(new_socket, sendDatabase, strlen(sendDatabase), 0);
              bzero(sendDatabase, sizeof(sendDatabase));
              bzero(buff, sizeof(buff));
            }
          }
          else
          {
            printf("ini query 3 %s", query_list[total - 3]);
            if (strcmp(query_list[total - 3], "WHERE") != 0)
            {
              char create_table[20000];
              snprintf(create_table, sizeof create_table, "databases/%s/%s", used_db, query_list[total - 1]);
              printf("buat table = %s", create_table);
              printf("tanpa where");
              int index[100];
              int iteration = 0;
              for (int i = 1; i < total - 2; i++)
              {
                index[iteration] = findColumn(create_table, query_list[i]);
                printf("%d\n", index[iteration]);
                iteration++;
              }
            }
            else if (strcmp(query_list[total - 3], "WHERE") == 0)
            {
              printf("dengan where");
            }
          }
        }
        else if (strcmp(cmd[0], "log") == 0)
        {
          writelog(cmd[1], cmd[2]);
          char warning[] = "\n";
          send(new_socket, warning, strlen(warning), 0);
          bzero(buff, sizeof(buff));
        }
        if (strcmp(buff, ":exit") == 0)
        {
          printf("Disconnected from %s:%d\n", inet_ntoa(newAddr.sin_addr), ntohs(newAddr.sin_port));
          break;
        }
        else
        {
          printf("Client: %s\n", buff);
          send(new_socket, buff, strlen(buff), 0);
          bzero(buff, sizeof(buff));
        }
      }
    }
  }

  close(new_socket);

  return 0;
}
```
Ada tiga struct yang digunakan: 
- tabel menyimpan informasi tentang tabel
- izin untuk menyimpan informasi izin pengguna
- izin_db untuk menyimpan informasi izin basis data.

Penjelasan fungsi:  
- Fungsi `isUserExist()` memeriksa apakah pengguna dengan nama tertentu 
ada di file database pengguna dengan cara memeriksa file `user.dat` yang 
berisi data user yang telah dibuat.
- Fungsi `createUser()` membuat entri pengguna baru di file database pengguna
dengan melakukan write nama dan password kepada `user.dat`.
- Fungsi `DBPermission()` memeriksa apakah pengguna memiliki izin untuk
mengakses database tertentu. Pertama-tama file `permission.dat` akan dibaca
lalu akan dicari kecocokan antara input nama dan database dengan suatu baris
pada file `permission.dat`. Apabila terdapat kecocokan maka program akan
mengembalikan nilai 1 dan jika tidak akan mengembalikan nilai 0.
- Fungsi `insertPermission()` menyisipkan entri izin baru ke dalam file 
database izin. Hal ini dilakukan dengan melakukan operasi write pada 
`permission.dat` untuk nama dan database yang dimasukkan.
- Fungsi `findColumn()` digunakan untuk menemukan kolom database. Hal ini
dilakukan dengan cara membuka file table dan membacanya. Kemudian mencocokkan
suatu nama column yang ada di file table dengan brute force. Apabila tidak ada
yang cocok maka fungsi akan mengembalikan nilai -1. Jika ada maka fungsi akan
mengembalikan index yang terdapat kolom yang cocok.
- Fungsi `deleteColumn()` digunakan untuk menghapus kolom. Caranya mirip dengan
`findColumn`, hanya saja karena suatu kolom akan dihapus, maka tabel harus
diperbarui. Caranya adalah dengan membuka file tabel yang ingin dihapus
kolomnya, lalu ditraverse. Fungsi membuat sebuah variabel tabel sementara
yang akan berisi kolom yang ditraverse. Untuk kolom yang namanya memiliki 
kecocokan, kolomnya tidak dimasukkan ke dalam tabel sementara. Sehingga hasil
akhirnya adalah seakan-akan kolom sudah dihapus dari tabel dan tabel sementara
akan menggantikan tabel asli.
- Fungsi `deleteTable()` digunakan untuk menghapus tabel. Hampir sama dengan
`deleteColumn()` tetapi untuk tabel. Tabel yang ada akan dihapus dan dibuat
ulang sehingga baris-baris yang ada di tabel sebelumnya akan terhapus.
- Fungsi `updateColumn()` digunakan untuk memperbarui nilai kolom. Fungsi ini
membuka file `table`, lalu akan melakukan traverse untuk semua baris. Apabila
nilai kolom yang ingin diganti indexnya sama degan variabel `index`, maka
fungsi akan melakukan update kepada kolom tersebut.
akan melakukan update kepada column yang memiliki index yang sama dengan nilai
`ganti`.
- Fungsi `updateColumnWhere()` digunakan untuk memperbarui nilai kolom yang 
memenuhi kondisi `WHERE`. Fungsi ini akan mengupdate kolom yang memenuhi
kondisi `WHERE`.
- Fungsi `deleteTableWhere()` digunakan untuk menghapus tabel yang memenuhi 
kondisi `WHERE`. Ketika traverse tabel, fungsi akan menandai tabel yang
memenuhi kondisi `WHERE` dan tidak akan ikut di write ke tabel baru.
- Fungsi `writelog()` digunakan untuk menulis entri log yang berisi perintah 
yang dieksekusi dan informasi pengguna ke file log. Fungsi akan mengambil
waktu saat ini lalu membuat sebuah laporan berisi perintah dan waktu perintah
tersebut dibuat dan memasukkannya ke dalam file `logUser.log`.
- Fungsi `main()` menyiapkan socket dan membuat sambungan ke port 8080.
Setelah itu, fungsi ini akan mendengarkan pesan yang dikirimkan client melalui
program client.c, dan menangani permintaan client. Lalu tergantung dari
perintah yang dikirimkan client, database akan menjalankan perintah sql yang
telah terdaftar diatas. Fungsi main juga memiliki error handling untuk setiap
perintah sql apabila ada masalah seperti format input yang salah atau tidak
memiliki izin.


## client_dump.c

```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>
#include <time.h>

#define PORT 8080

// deklarasi struct
struct acces
{
    char name[10000];
    char pass[10000];
};

// untuk memeriksa permission
int checkPermission(char *uname, char *pass)
{
    FILE *file;
    struct acces user;
    int id, mark = 0;

    file = fopen("../database/databases/user.dat", "rb");

    while (1)
    {
        fread(&user, sizeof(user), 1, file);
        if (strcmp(user.name, uname) == 0)
            if (strcmp(user.pass, pass) == 0)
                mark = 1;
        if (feof(file))
            break;
    }

    fclose(file);

    if (mark == 0)
    {
        printf("Not Allowed: Not have permission\n");
        return 0;
    }
    else
        return 1;
}

int main(int argc, char *argv[])
{
    int allowed = 0;
    int id_user = geteuid();
    char used_db[1000];

    if (geteuid() == 0)
        allowed = 1;
    else
    {
        int id = geteuid();
        allowed = checkPermission(argv[2], argv[4]);
    }

    if (allowed == 0)
        return 0;

    FILE *file;
    char loc[10000];
    snprintf(loc, sizeof loc, "../database/log/log%s.log", argv[2]);
    file = fopen(loc, "rb");
    char buff[20000];
    char cmd[100][10000];
    int mark = 0;

    while (fgets(buff, 20000, file))
    {
        char *token;
        char temp_buff[20000];
        snprintf(temp_buff, sizeof temp_buff, "%s", buff);
        token = strtok(temp_buff, ":");
        int i = 0;

        while (token != NULL)
        {
            strcpy(cmd[i], token);
            i++;
            token = strtok(NULL, ":");
        }

        char *b = strstr(cmd[4], "USE");
        if (b != NULL)
        {
            char *tokens;
            char temp_cmd[20000];
            strcpy(temp_cmd, cmd[4]);
            tokens = strtok(temp_cmd, "; ");
            int j = 0;

            char use_cmd[100][10000];

            while (tokens != NULL)
            {
                strcpy(use_cmd[j], tokens);
                j++;
                tokens = strtok(NULL, "; ");
            }

            char databaseTarget[20000];
            if (strcmp(use_cmd[1], argv[5]) == 0)
                mark = 1;
            else
                mark = 0;
        }
        if (mark == 1)
            printf("%s", buff);
    }
    fclose(file);
}
```
Terdapat 1 struct:
- `acces` untuk menyimpan nama pengguna dan kaat sandi

Penjelasan fungsi:

- Fungsi `checkPermission()` digunakan untuk 
memeriksa izin akses pengguna berdasarkan username (uname) dan password (pass)
yang diberikan. Fungsi ini membuka file "user.dat" yang berisi informasi akses 
pengguna, kemudian memeriksa apakah username dan password yang diberikan cocok 
dengan entri yang ada dalam file tersebut. Jika cocok, maka mark akan diubah 
menjadi 1 yang menandakan izin akses diberikan.
- Fungsi `main()` main merupakan titik masuk utama program. Pada awalnya,
variabel allowed diinisialisasi dengan 0, yang menandakan bahwa akses belum 
diizinkan. Kemudian, program memeriksa apakah pengguna menjalankan program 
dengan hak akses superuser (UID 0) atau tidak. Jika pengguna adalah superuser,
maka variabel allowed diubah menjadi 1, sehingga akses diizinkan. Jika bukan 
superuser, program memanggil fungsi checkPermission untuk memeriksa izin akses 
pengguna berdasarkan argumen yang diberikan. Jika izin akses diberikan, 
allowed diubah menjadi 1, jika tidak, program berakhir.

## Dockerfile
```
FROM ubuntu:latest

#update package
RUN apt-get update && apt-get install -y gcc

#copy database dan client ke container
COPY database/database.c .
COPY client/client.c .

#meng-compile database.c pada docker image
RUN gcc database.c -o database

#meng-compile database.c pada docker image
RUN gcc client.c -o client

#menjalankan kedua file
CMD sh -c './database & ./client'
```

Dockerfile ini menggunakan base image ubuntu:latest. Untuk mengcompile kodenya
diperlukan gcc, oleh karena itu gcc di install. Lalu kedua kode `database.c` 
dan `client.c` akan disalin ke container. Setelah itu, kedua kode dapat 
dicompile dan dijalankan

## docker-compose.yaml
```
version: '3'

services:
  storage-app:
    image: ngurahervan/fp
    deploy:
      replicas: 5
```

Docker compose akan mendeploy 5 replika dari image yang dibuat dari dockerfile sebelumnya
