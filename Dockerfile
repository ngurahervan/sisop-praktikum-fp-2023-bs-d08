FROM ubuntu:latest

#update package
RUN apt-get update && apt-get install -y gcc

#copy database dan client ke container
COPY database/database.c .
COPY client/client.c .

#meng-compile database.c pada docker image
RUN gcc database.c -o database

#meng-compile database.c pada docker image
RUN gcc client.c -o client

#menjalankan kedua file
CMD sh -c './database & ./client'
