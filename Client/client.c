#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>
#include <time.h>

#define PORT 8080

struct acces {
  char name[10000];
  char pass[10000];
};

int autentikasi(char *username, char *password) {
  FILE *file;
  struct acces client;
  int id, found = 0;
  file = fopen("../database/databases/user.dat", "rb");

  while (fread(&client, sizeof(client), 1, file)) {
    if (strcmp(client.name, username) == 0 && strcmp(client.pass, password) == 0) {
      found = 1;
      break;
    }
  }

  fclose(file);

  if (found == 0) {
    printf("Not Allowed: No permission\n");
    return 0;
  } else {
    return 1;
  }
}

void HistoryLog(char *command, char *name) {
  time_t times;
  struct tm *info;
  time(&times);
  info = localtime(&times);

  char log_entry[1000];

  FILE *file;
  char loc[10000];
  snprintf(loc, sizeof(loc), "../database/log/log%s.log", name);
  file = fopen(loc, "ab");

  sprintf(log_entry, "%d-%.2d-%.2d %.2d:%.2d:%.2d:%s:%s;\n", info->tm_year + 1900, info->tm_mon + 1, info->tm_mday, info->tm_hour, info->tm_min, info->tm_sec, name, command);

  fputs(log_entry, file);
  fclose(file);
}

int main(int argc, char *argv[]) {
  int allowed = 0;
  int user_id = geteuid();
  char used_db[1000];

  if (geteuid() == 0)
    allowed = 1;
  else
    allowed = autentikasi(argv[2], argv[4]);

  if (allowed == 0)
    return 0;

  int client_socket, ret;
  struct sockaddr_in server_addr;
  char buffer[32000];

  client_socket = socket(AF_INET, SOCK_STREAM, 0);

  if (client_socket < 0) {
    printf("[-]Error creating socket.\n");
    exit(1);
  }

  printf("[+]Client Socket created.\n");

  memset(&server_addr, '\0', sizeof(server_addr));
  server_addr.sin_family = AF_INET;
  server_addr.sin_port = htons(PORT);
  server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");

  ret = connect(client_socket, (struct sockaddr *)&server_addr, sizeof(server_addr));

  if (ret < 0) {
    printf("[-]Error connecting.\n");
    exit(1);
  }
  printf("[+]Connected to the server.\n");

  while (1) {
    printf("Client: \t");
    char input[10000];
    char temp[10000];
    char cmd[100][10000];
    char *token;
    int i = 0;
    scanf(" %[^\n]s", input);
    strcpy(temp, input);
    token = strtok(input, " ");

    while (token != NULL) {
      strcpy(cmd[i], token);
      i++;
      token = strtok(NULL, " ");
    }

    int false_cmd = 0;
    //CREATE USER
    if (strcmp(cmd[0], "CREATE") == 0) {
      if (strcmp(cmd[1], "USER") == 0 && strcmp(cmd[3], "IDENTIFIED") == 0 && strcmp(cmd[4], "BY") == 0) {
        snprintf(buffer, sizeof(buffer), "cUser:%s:%s:%d", cmd[2], cmd[5], user_id);
        send(client_socket, buffer, strlen(buffer), 0);
      }
    //CREATE DATABASE
      else if (strcmp(cmd[1], "DATABASE") == 0) {
        snprintf(buffer, sizeof(buffer), "cDatabase:%s:%s:%d", cmd[2], argv[2], user_id);
        send(client_socket, buffer, strlen(buffer), 0);
      }
    //CREATE TABLE
      else if (strcmp(cmd[1], "TABLE") == 0) {
        snprintf(buffer, sizeof(buffer), "cTable:%s", temp);
        send(client_socket, buffer, strlen(buffer), 0);
      }
    }
    //GRANT PERMISSION
    else if (strcmp(cmd[0], "GRANT") == 0 && strcmp(cmd[1], "PERMISSION") == 0 && strcmp(cmd[3], "INTO") == 0) {
      snprintf(buffer, sizeof(buffer), "gPermission:%s:%s:%d", cmd[2], cmd[4], user_id);
      send(client_socket, buffer, strlen(buffer), 0);
    }
    // USE
    else if (strcmp(cmd[0], "USE") == 0) {
      snprintf(buffer, sizeof(buffer), "uDatabase:%s:%s:%d", cmd[1], argv[2], user_id);
      send(client_socket, buffer, strlen(buffer), 0);
    }
    // cekCurrentDatabase
    else if (strcmp(cmd[0], "cekCurrentDatabase") == 0) {
      snprintf(buffer, sizeof(buffer), "%s", cmd[0]);
      send(client_socket, buffer, strlen(buffer), 0);
    }
    //DROP 
    else if (strcmp(cmd[0], "DROP") == 0) {
      if (strcmp(cmd[1], "DATABASE") == 0) {
        snprintf(buffer, sizeof(buffer), "dDatabase:%s:%s", cmd[2], argv[2]);
        send(client_socket, buffer, strlen(buffer), 0);
      }
      else if (strcmp(cmd[1], "TABLE") == 0) {
        snprintf(buffer, sizeof(buffer), "dTable:%s:%s", cmd[2], argv[2]);
        send(client_socket, buffer, strlen(buffer), 0);
      }
      else if (strcmp(cmd[1], "COLUMN") == 0) {
        snprintf(buffer, sizeof(buffer), "dColumn:%s:%s:%s", cmd[2], cmd[4], argv[2]);
        send(client_socket, buffer, strlen(buffer), 0);
      }
    }
    //INSERT
    else if (strcmp(cmd[0], "INSERT") == 0 && strcmp(cmd[1], "INTO") == 0) {
      snprintf(buffer, sizeof(buffer), "insert:%s", temp);
      send(client_socket, buffer, strlen(buffer), 0);
    }
    //UPDATE
    else if (strcmp(cmd[0], "UPDATE") == 0) {
      snprintf(buffer, sizeof(buffer), "update:%s", temp);
      send(client_socket, buffer, strlen(buffer), 0);
    }
    // DELETE
    else if (strcmp(cmd[0], "DELETE") == 0) {
      snprintf(buffer, sizeof(buffer), "delete:%s", temp);
      send(client_socket, buffer, strlen(buffer), 0);
    }
    // SELECT
    else if (strcmp(cmd[0], "SELECT") == 0) {
      snprintf(buffer, sizeof(buffer), "select:%s", temp);
      send(client_socket, buffer, strlen(buffer), 0);
    }
    //EXIT
    else if (strcmp(cmd[0], ":exit") != 0) {
      false_cmd = 1;
      char warning[] = "Invalid Command";
      send(client_socket, warning, strlen(warning), 0);
    }

    if (false_cmd != 1) {
      char sender[10000];
      if (user_id == 0)
        strcpy(sender, "root");
      else
        strcpy(sender, argv[2]);
      HistoryLog(temp, sender);
    }

    if (strcmp(cmd[0], ":exit") == 0) {
      send(client_socket, cmd[0], strlen(cmd[0]), 0);
      close(client_socket);
      printf("[-]Disconnected from server.\n");
      exit(1);
    }
    bzero(buffer, sizeof(buffer));
    if (recv(client_socket, buffer, 1024, 0) < 0)
      printf("[-]Error receiving data.\n");
    else
      printf("Server: \t%s\n", buffer);
  }

  return 0;
}
